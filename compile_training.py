import sys;
import glob;
import re;
dir1 = sys.argv[1];
dir2 = sys.argv[2];
files = glob.glob("./"+dir1+"/*.txt");
output = open(dir2, 'w');
files = sorted(files);
for file1 in files:
    #print(file1);
    ifile = open(file1, 'r', errors='ignore' );
    Obj = re.match(r'.*/(.*?)\..*\.txt', file1);
    subject = Obj.group(1); 
    output.write(subject+' subject: ');
    for line in ifile:
      line.replace('Subject: ','');
      output.write(line.rstrip('\n')+' '); 
    output.write('\n');

