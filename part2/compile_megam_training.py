import sys;
import re;
import math;
import json;
if (len(sys.argv)!=4):
   sys.exit('Error: you have to give both the training file and model file!');
training_file = sys.argv[1];
output_file = sys.argv[2];
word_file   = sys.argv[3];
file1 = open(training_file, 'r');
file2 = open(output_file, 'w');
file3 = open(word_file, 'w');
word_to_index = {};
current_index = 1;
for line in file1:
    temp_word_count = {};
    line.rstrip('\n');
    reGroup = re.match("^(\w+) subject: (.*)$",line, re.I);
    if(reGroup):
      subject = reGroup.group(1);
      content = reGroup.group(2);
    else:
      print("One illegal line!!\n");
    words = re.split('\W+', content);
    for word in words:
      if not word: continue;
      else:
        if word in word_to_index:
           if str(word_to_index[word]) not in temp_word_count.keys(): temp_word_count[str(word_to_index[word])]=0;
           temp_word_count[str(word_to_index[word])] += 1;
        else:
           word_to_index[word] = current_index;
           current_index +=1;
           temp_word_count[str(word_to_index[word])] = 1;
    if subject == 'HAM' or subject=='NEG':  
       file2.write("0 ");
    else: file2.write("1 ");
    for key in sorted(temp_word_count.keys(), key=int):
        file2.write(str(key)+" "+str(temp_word_count[key])+' ');
    file2.write('\n');
file3.write(json.dumps(word_to_index));         










        
