import sys;
import re;
if len(sys.argv) != 4:
   exit("The input is not illegal!!");
classified_file = sys.argv[1];
test_file = sys.argv[2];
out_file = sys.argv[3];
file1 = open(classified_file, 'r');
file2 = open(test_file, 'r');
file3 = open(out_file, 'w');
if(re.match('^sentiment',classified_file)):
  pos_string = 'POS';
  neg_string = 'NEG';
if(re.match('^spam',classified_file)):
  pos_string = 'SPAM';
  neg_string = 'HAM' ;
true_positive =0;
true_negative =0;
classified_positive =0;
classified_negative =0;
shared_positive =0;
shared_negative =0;
result = [];
for line in file1:
    line.rstrip('\n');
    words = line.split('\t');
    if(float(words[0])>0): 
        result.append('+1');
        file3.write(pos_string+'\n');        
    else: 
        result.append('-1');
        file3.write(neg_string+'\n');
i = 0; 
for line in file2:
    line.rstrip('\n');
    words = line.split(' ');
    if(words[0]=='+1' or words[0]=='1'):
        true_positive+=1;
        if(result[i]=='+1'):
           classified_positive+=1;
           shared_positive+=1;
        else:
           classified_negative+=1;
    else:
        true_negative+=1;
        if(result[i]=='-1'):
           classified_negative+=1;
           shared_negative+=1;
        else:
           classified_positive+=1;
    i+=1;
"""
spam_precision = float(shared_positive)/classified_positive;
spam_recall    = float(shared_positive)/true_positive;
spam_f         = 2*spam_precision*spam_recall/(spam_precision+spam_recall);
print("TP: "+str(true_positive));
print("CP: "+str(classified_positive));
print("SP: "+str(shared_positive));
print("TN: "+str(true_negative));
print("CN: "+str(classified_negative));
print("SN: "+str(shared_negative));

print(pos_string+" Precision: "+ str(spam_precision));
print(pos_string+" Recall: "+ str(spam_recall));
print(pos_string+" F Score: "+str(spam_f));
ham_precision = float(shared_negative)/classified_negative;
ham_recall    = float(shared_negative)/true_negative;
ham_f         = 2*ham_precision*ham_recall/(ham_precision+ham_recall);
print(neg_string+" Precision: "+ str(ham_precision));
print(neg_string+" Recall: "+ str(ham_recall));
print(neg_string+" F Score: "+str(ham_f));
"""
