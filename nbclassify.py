import sys;
import re;
import json;
import math;
if len(sys.argv) != 3:
   exit("The input is not illegal!!");
model_file= sys.argv[1];
test_file = sys.argv[2];
if(re.match('^sentiment',model_file)):
  pos_string = 'POS';
  neg_string = 'NEG';
if(re.match('^spam',model_file)):
  pos_string = 'SPAM';
  neg_string = 'HAM' ;
file1 = open(model_file, 'r');
file2 = open(test_file,  'r', errors='ignore');
model = json.load(file1);
true_positive =0;
true_negative =0;
classified_positive =0;
classified_negative =0;
shared_positive =0;
shared_negative =0;
for line in file2:
    spam_score = model['spam_prior'];
    ham_score  = model['ham_prior'];
    line=line.rstrip('\n');
    reGroup = re.match('^(\w+) subject: (.*)$',line,re.I);
    if reGroup:
      subject = reGroup.group(1);
      content = reGroup.group(2);
    else:
      print('Warning: one illegal line!!');
      continue;
    words = re.split('\W+',content);
    for word in words:
       if not word: continue;
       if word in model['ham_features']:
          ham_score += model['ham_features'][word];
       else:
          ham_score += math.log10(1.0/model['ham_word_count']);
       if word in model['spam_features']:
          spam_score += model['spam_features'][word];
       else:
          spam_score += math.log10(1.0/model['spam_word_count']);
    if(pos_string=="SPAM"):  
       if(spam_score > ham_score):
         result = pos_string; 
         print(pos_string);
       else: 
         result = neg_string;
         print(neg_string);
    else:
       if(spam_score >= ham_score):
         result = pos_string; 
         print(pos_string);
       else: 
         result = neg_string;
         print(neg_string);
    if(subject==pos_string):
       true_positive += 1;
       if(result==pos_string):
         shared_positive +=1;classified_positive +=1;
       else:
         classified_negative +=1;
    if(subject==neg_string):
       true_negative += 1;
       if(result==neg_string):
         shared_negative +=1;classified_negative +=1;
       else:
         classified_positive +=1;
"""
spam_precision = float(shared_positive)/classified_positive;
spam_recall    = float(shared_positive)/true_positive;
spam_f         = 2*spam_precision*spam_recall/(spam_precision+spam_recall);
print("TP: "+str(true_positive));
print("CP: "+str(classified_positive));
print("SP: "+str(shared_positive));
print("TN: "+str(true_negative));
print("CN: "+str(classified_negative));
print("SN: "+str(shared_negative));

print(pos_string+" Precision: "+ str(spam_precision));
print(pos_string+" Recall: "+ str(spam_recall));
print(pos_string+" F Score: "+str(spam_f));
ham_precision = float(shared_negative)/classified_negative;
ham_recall    = float(shared_negative)/true_negative;
ham_f         = 2*ham_precision*ham_recall/(ham_precision+ham_recall);
print(neg_string+" Precision: "+ str(ham_precision));
print(neg_string+" Recall: "+ str(ham_recall));
print(neg_string+" F Score: "+str(ham_f));
"""





