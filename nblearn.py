import sys;
import re;
import math;
import json;
if (len(sys.argv)!=3):
   sys.exit('Error: you have to give both the training file and model file!');
training_file = sys.argv[1];
model_file = sys.argv[2];
file1 = open(training_file, 'r');
file2 = open(model_file, 'w');
spam_num = 0; ham_num = 0;
word_check_all = {};
word_count_spam = {};
word_count_ham = {};
for line in file1:
    line.rstrip('\n');
    reGroup = re.match("^(\w+) subject: (.*)$",line, re.I);
    if(reGroup):
      subject = reGroup.group(1);
      content = reGroup.group(2);
    else:
      print("One illegal line!!\n");
    words = re.split('\W+', content);
    if subject == 'HAM' or subject=='NEG':  
       ham_num+=1;
       for word in words:
           if not word: continue;
           if word not in word_count_ham: word_count_ham[word] = 1;
           else: word_count_ham[word]+=1;
           word_check_all[word] = 1;
    else: 
       spam_num+=1;
       for word in words:
           if not word: continue;
           if word not in word_count_spam: word_count_spam[word] = 1;
           else: word_count_spam[word]+=1;  
           word_check_all[word] = 1;
ham_prior = math.log10(float(ham_num)/(ham_num + spam_num));
spam_prior= math.log10(float(spam_num)/(ham_num+ spam_num));
num_types = len(word_check_all);
total_ham_num = sum(word_count_ham.values());
total_spam_num= sum(word_count_spam.values());
ham_word_cpd = {};
spam_word_cpd= {};
for word in word_check_all:
    if word in word_count_ham: 
       ham_word_cpd[word] = math.log10(float(word_count_ham[word]+1)/(total_ham_num+num_types));
    else:
       ham_word_cpd[word] = math.log10(1.0/(total_ham_num+num_types));       
    if word in word_count_spam: 
       spam_word_cpd[word] = math.log10(float(word_count_spam[word]+1)/(total_spam_num+num_types));
    else:
       spam_word_cpd[word] = math.log10(1.0/(total_spam_num+num_types));       
    

output = {};
output['ham_prior'] = ham_prior;
output['spam_prior']= spam_prior;
output['ham_features'] = ham_word_cpd;
output['spam_features']= spam_word_cpd;
output['ham_word_count'] = total_ham_num+num_types;
output['spam_word_count'] = total_spam_num+num_types;
#if "" in ham_word_cpd: print 'yes';
file2.write(json.dumps(output));






        
