1) In Part 1:
For spam dataset:
SPAM Precision: 0.9567567567567568
SPAM Recall: 0.9752066115702479
SPAM F Score: 0.965893587994543
HAM Precision: 0.9909365558912386
HAM Recall: 0.984
HAM F Score: 0.9874560963371801

For sentiment dataset, 1002 files are selected as devlopment set, 501 for POS and 501 for NEG:
POS Precision: 0.7911547911547911
POS Recall: 0.6427145708582834
POS F Score: 0.7092511013215859
NEG Precision: 0.6991596638655462
NEG Recall: 0.8303393213572854
NEG F Score: 0.7591240875912408

2) In Part 2:
Using SVM:
For spam dataset: 
SPAM Precision: 0.9690402476780186
SPAM Recall: 0.8622589531680441
SPAM F Score: 0.9125364431486881
HAM Precision: 0.9519230769230769
HAM Recall: 0.99
HAM F Score: 0.9705882352941176

For sentiment dataset:
POS Precision: 0.8520710059171598
POS Recall: 0.8622754491017964
POS F Score: 0.8571428571428571
NEG Precision: 0.8606060606060606
NEG Recall: 0.8502994011976048
NEG F Score: 0.855421686746988


Using Maximum Entropy
For spam dataset:
SPAM Precision: 0.962536023054755
SPAM Recall: 0.9201101928374655
SPAM F Score: 0.9408450704225352
HAM Precision: 0.9714566929133859
HAM Recall: 0.987
HAM F Score: 0.9791666666666667

For sentiment dataset:
POS Precision: 0.6388206388206388
POS Recall: 0.5189620758483033
POS F Score: 0.5726872246696034
NEG Precision: 0.5949579831932773
NEG Recall: 0.7065868263473054
NEG F Score: 0.645985401459854

3) The precision, recall and F-scoore decreases in all the situations, because the 10% of the training data will make the learning process not able to consider most of the training data, some words are not considered at all which have important roles in test data.















